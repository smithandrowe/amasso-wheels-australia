<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

<?php wp_footer(); ?>
<div id="footer" class="row">
	<div class="col-xs-6">
		<a class="fb" href="https://www.facebook.com/pages/Am%C3%A1sso-Wheels/311319702340189" target="_blank"><img src="<?php echo bloginfo('template_url'); ?>/images/fb.png" alt="Amasso Wheels" /></a>
	</div>
	<div class="col-xs-3">
		<a class="bboss" href="http://www.bbosswheels.com" target="_blank"><img src="<?php echo bloginfo('template_url'); ?>/images/bboss.png" alt="Amasso Wheels" /></a>
	</div>
	<div class="col-xs-3">
		<a class="vossen" href="http://www.vossenwheels.com" target="_blank"><img src="<?php echo bloginfo('template_url'); ?>/images/vossen.png" alt="Amasso Wheels" /></a>
	</div>
	
</div><!--end row -->
</div><!--end container -->
</div><!--end main -->
</body>
</html>