<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>
</div>
<div class="main">
	
	<div class="container">
		<div id="video-wrapper" class="row">
			<div class="col-xs-12">
				<div class="video">
					<iframe width="420" height="315" src="//www.youtube.com/embed/W-SaqwdPsGQ?wmode=opaque" frameborder="0" allowfullscreen></iframe>	
				</div>
			</div>
			<div class="wheels">
				<div class="col-xs-4">
					<div class="idemo"><img src="<?php echo bloginfo('template_url'); ?>/images/wheels/wheel-idemo.png" class="img-responsive" alt="Amasso Wheels"></div>
				</div>
				<div class="col-xs-4">
					<div class="trinidy"><img src="<?php echo bloginfo('template_url'); ?>/images/wheels/wheel-trinidy.png" class="img-responsive" alt="Amasso Wheels"></div>
				</div>
				<div class="col-xs-4">
					<div class="venix"><img src="<?php echo bloginfo('template_url'); ?>/images/wheels/wheel-venix.png" class="img-responsive" alt="Amasso Wheels"></div>
				</div>
				<div class="clearfix"></div>
				<div id="idemo" class="col-xs-12">
					<img src="<?php echo bloginfo('template_url'); ?>/images/wheels/wheel-idemo-hover.png" class="img-responsive" alt="Amasso Wheels">
				</div>
				<div id="trinidy" class="col-xs-12">
					<img src="<?php echo bloginfo('template_url'); ?>/images/wheels/wheel-trinidy-hover.png" class="img-responsive" alt="Amasso Wheels">
				</div>
				<div id="venix" class="col-xs-12">
					<img src="<?php echo bloginfo('template_url'); ?>/images/wheels/wheel-venix-hover.png" class="img-responsive" alt="Amasso Wheels">
				</div>
				<div class="col-xs-12">
					<a data-toggle="modal" href="#contact" class="btn btn-amasso">Enquiries and Preorders</a>
				</div>
				<div class="modal fade" id="contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				    <div class="modal-dialog">
				      <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				          <h4 class="modal-title">Enquiries and Pre Orders:</h4>
				        </div>
				        <div class="modal-body">
				        	<div class="row">
				        		
				        			<?php echo do_shortcode("[contact-form-7 id='4' title='Enquiries and Pre Orders']"); ?>		
				        	
				        	</div>
				          
				        </div>
				        
				      </div><!-- /.modal-content -->
				    </div><!-- /.modal-dialog -->
				  </div><!-- /.modal -->
			</div>
		</div>
<?php get_footer(); ?>