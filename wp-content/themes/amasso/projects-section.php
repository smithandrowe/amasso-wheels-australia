<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Colouralia
 * @subpackage Emily Armstrong
 */
$args = array(
	'posts_per_page'   => -1,
	'offset'           => 0,
	'category'         => '5',
	'orderby'          => 'post_date',
	'order'            => 'DESC',
	'include'          => '',
	'exclude'          => '',
	'meta_key'         => '',
	'meta_value'       => '',
	'post_type'        => 'post',
	'post_mime_type'   => '',
	'post_parent'      => '',
	'post_status'      => 'publish',
	'suppress_filters' => true ); 
	
$projects = get_posts( $args );

?>

<div id="projects">
	<div class="infinite">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="title">
						<h2>Projects</h2>
					</div>
				</div>
				<?php if (count($projects) > 0) { ?>
					
					<?php foreach ($projects as $project) { ?>
						<?php if (count($projects) == 1) { ?>
							<div class="col-xs-12">	
						<?php }else { ?>
							<div class="col-xs-4">
						<?php } ?>
							<div id="#post-data-<?php echo $project->ID; ?>"class="project">
								<?php echo get_the_post_thumbnail( $project->ID, 'full'); ?>
								<div class="project-link">
									<a><?php echo $project->post_title; ?></a>
								</div>
							</div>
						</div>
						
					<?php } ?>
					<div class="clearfix"></div>
				<?php }else{ ?>
				<div class="col-xs-12">
					<h2>Coming Soon</h2>
				</div>	
				<?php } ?>
				<?php if (count($projects) > 0) { ?>
					
					
						<?php foreach ($projects as $project) { ?>
							<div id="post-data-<?php echo $project->ID; ?>" class="project-body">
						<div class="col-xs-6">
							<?php echo get_the_post_thumbnail( $project->ID, 'full'); ?>
						</div>	
						<div class="col-xs-6">
							<div  class="project">
								
								<div class="project-content">
									<?php echo $project->post_content; ?>
								</div>
								<!--<span class="<?php echo $project->ID; ?>" href="#">Close</span>-->
							
							</div>
							
						</div>
						
					<?php } ?>
					</div>
					
				<?php } ?>					
			</div>
		</div>
	</div>
</div>
